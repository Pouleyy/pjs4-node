/* SQLINES EVALUATION VERSION TRUNCATES VARIABLE NAMES AND COMMENTS. */
/* OBTAIN A LICENSE AT WWW.SQLINES.COM FOR FULL CONVERSION. THANK YOU. */

create table users(
	idUser INT not null AUTO_INCREMENT, 
	idFacebook TEXT,
	tokenFacebook TEXT,
	lastActivity datetime,
	PRIMARY KEY(idUser)
);

create table conversation(
	idConv INT not null AUTO_INCREMENT,
	nameConv varchar(50),
	PRIMARY KEY(idConv)
);
	
create table groups(
	idGroup INT not null AUTO_INCREMENT,
	nameGroup varchar(50),
	presentation TEXT,
	idLeader INT not null,
	finish BOOLEAN DEFAULT 0,
	idConv INT not null,
	FOREIGN KEY (idLeader) REFERENCES users(idUser) ON DELETE CASCADE,
	FOREIGN KEY (idConv) REFERENCES conversation(idConv) ON DELETE CASCADE,
	PRIMARY KEY(idGroup)
);
		
create table skill(
	idSkill INT not null AUTO_INCREMENT,
	nameSkill varchar(50),
	PRIMARY KEY(idSkill)
);
	
create table ownSkill(
	idUser INT not null,
	idSkill INT not null,
	FOREIGN KEY (idUser) REFERENCES users(idUser) ON DELETE CASCADE,
	FOREIGN KEY (idSkill) REFERENCES skill(idSkill) ON DELETE CASCADE,
	PRIMARY KEY(idUser, idSkill)
);
	
create table message(
	idMsg INT not null AUTO_INCREMENT,
	string varchar(500),
	idConv INT not null,
	idUser INT not null,
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(idMsg),
	foreign key(idConv) references conversation(idConv) ON DELETE CASCADE,
	foreign key(idUser) references users(idUser) ON DELETE CASCADE
);
		
create table accesConv(
	idConv INT not null,
	idUser INT not null,
	foreign key(idUser) references users(idUser) ON DELETE CASCADE,
	foreign key(idConv) references conversation(idConv) ON DELETE CASCADE,
	primary key(idUser,idconv)
);

create table note(
	idUserFrom INT not null,
	idUserTo INT not null,
	idGroup INT not null,
	note INT,
	foreign key(idUserFrom) references users(idUser) ON DELETE CASCADE,
	foreign key(idUserTo) references users(idUser) ON DELETE CASCADE,
	foreign key(idGroup) references groups(idGroup) ON DELETE CASCADE,
	primary key(idUserFrom,idUserTo,idGroup)
);

create table relation(
	idUserFrom INT not null,
	idUserTo INT not null,
	relation FLOAT,
	foreign key(idUserFrom) references users(idUser) ON DELETE CASCADE,
	foreign key(idUserTo) references users(idUser) ON DELETE CASCADE,
	primary key(idUserFrom,idUserTo)
);

create table accept(
	idUserFrom INT not null,
	idUserTo INT not null,
	accept boolean,
	foreign key(idUserFrom) references users(idUser) ON DELETE CASCADE,
	foreign key(idUserTo) references users(idUser) ON DELETE CASCADE,
	primary key(idUserFrom,idUserTo)
);

create table profil(
	idUser INT not null,
	firstname varchar(50),
	lastname varchar(50),
	email varchar(50),
	birthday TIMESTAMP,
	gender varchar(20),
	picture TEXT,
	foreign key(idUser) references users(idUser) ON DELETE CASCADE,
	primary key(idUser)
);

create table accessGroup(
	idUser INT not null,
	idGroup INT not null,
	foreign key(idUser) references users(idUser) ON DELETE CASCADE,
	foreign key(idGroup) references groups(idGroup) ON DELETE CASCADE,
	 primary key(idUser, idGroup)
);











	