ERREUR :

0 : Pas d'erreur
1 : Aucun utilisateur trouvé
2 : Aucun skill trouvé
3 : idFacebook non fourni
4 : Token manquant
5 : paramètres manquants
6 : Utilisateur déjà existant
7 : Aucun groupe trouvé
8 : User non leader
9 : L'utilisateur est déjà présent dans le groupe
10 : L'utilisateur n'est pas Leader du groupe
11 : Le leader ne peut pas se supprimer de son groupe
12 : Paramètres invalides
190 : Token invalide
25000 : Aucun token envoyé
