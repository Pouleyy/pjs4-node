var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var port = process.env.PORT || 8080;
var router = express.Router();


var users = require('./routes/users.js');
var profil = require('./routes/profil.js');
var group = require('./routes/group.js');
var match = require('./routes/match.js');
var skill = require('./routes/skill.js');
var message = require('./routes/message.js');

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

router.use(function(req, res,next){
  var modelUser = require('./models/users.js');
  console.log(req.method +" - " +req.url)
  if(req.url == '/user' && req.method == 'POST' && req.headers.token)
    next()
  else {
    modelUser.getUserByToken(req.headers.token, function (err, result) {
      if (err)
        return res.send(err);
      req.idUser = result.idUser;
      req.lastActivity = result.lastActivity;
      req.tokenFacebook = req.headers.token;
      next();
    })
  }
})

router.get('/', function(req, res) {
  res.json({ message: 'Successful connexion' });
});

users.route(router);
profil.route(router);
group.route(router);
match.route(router);
skill.route(router);
message.route(router);

app.use('/api', router);

app.listen(port);
console.log('Server launch on ' + port);
