var profil = require('../models/profil.js');


module.exports = {

    route: function (router) {
        router.route('/profil').put(function(req, res){
            profil.modifyProfil(req.idUser, req.body, function(err, result){
                if(err) {
                    res.json(err);
                }
                else {
                    res.json(result);
                }
            })
        });

        router.route('/profil/:idUser').get(function(req, res){
            profil.getProfil(req.idUser, req.params.idUser, function(err, result){
                if(err)
                    return res.json(err);
                else
                    return res.json(result);
            })
        });

        router.route('/profil/skill').post(function(req, res) {
            profil.addSkill(req.idUser, req.body.skill, function (err, result) {
                if (err)
                    return res.json(err);
                else
                    return res.json(result);
            })
        });

        router.route('/profil/skill').delete(function(req, res) {
            profil.deleteSkill(req.idUser, req.body.skill, function (err, result) {
                if (err)
                    return res.json(err);
                else
                    return res.json(result);
            })
        });

    }

}








