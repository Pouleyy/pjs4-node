var modelProfil = require('../models/profil.js');
var user = require('../models/users.js');
var facebook = require('../facebook/request.js')

module.exports = {
  route: function (router) {
    router.route('/user').post(function(req, res) {
      facebook.getProfil(req.headers.token, function (err, profil) {
        if (profil.error) {
          return res.send(profil.error);
        }
        else {
          user.createUser(req.body.idFacebook, req.headers.token, profil, function (err, response) {
            if (err) {
              return res.json(err);
            }
            else
              return res.json(response);
          });
        }
      });
    });

    router.route('/user').get(function(req, res){
        user.getUserByToken(req.headers.token, function(err, rows){
          if(err)
            return res.json(err);
          else
            return res.json(rows);
        })
    });

    router.route('/user').delete(function(req, res){
      user.deleteUser(req.idUser, function (err, result) {
        if (err)
          res.send(err);
        else
          res.json(result);
      });
    });

  }

}






