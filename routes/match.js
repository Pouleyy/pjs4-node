/**
 * Created by Killian on 23/02/2016.
 */
var match = require('../models/match.js');
var request = require('request');

module.exports = {
    route: function (router) {
        router.route('/match/suggestion').get(function (req, res) {
            match.getNextRelation(req.tokenFacebook, req.idUser, req.query.nbRelations, req.lastActivity, function (err, result) {
                if (err)
                    return res.json(err);
                else
                    return res.json(result);
            })
        });

        router.route('/match/suggestion').put(function(req, res){
            match.refreshRelation(req.headers.token, function(errReq, resReq, bodyReq){
                if(errReq)
                    return res.json(errReq)
                else
                    return res.json(bodyReq)
            })
        });

        router.route('/match/accept').post(function(req,res){
            match.accept(req.idUser, req.body.idUser, function(err, result){
                if(err)
                    res.json(err)
                else
                    res.json(result)
            })
        });

        router.route('/match/refuse').post(function(req,res){
            match.refuse(req.idUser, req.body.idUser, function(err, result){
                if(err)
                    res.json(err)
                else
                    res.json(result)
            })
        });

        router.route('/match').get(function(req,res){
            match.getMatchs(req.idUser, function(err, result){
                if(err)
                    res.json(err)
                else
                    res.json(result)
            })
        });
    }


}