var skill = require('../models/skill.js');
module.exports = {

    route: function (router) {
        router.route('/skill').get(function (req, res) {
            skill.getSkill(function (err, result) {
                if (err) {
                    res.json(err);
                }
                else {
                    res.json(result);
                }
            })
        });
    }
}