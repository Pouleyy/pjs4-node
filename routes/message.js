/**
 * Created by Killian on 28/03/2016.
 */
var message = require('../models/message.js');

module.exports = {
    route: function (router) {
        router.route("/conversations").get(function(req, res){
            message.getConversations(req.idUser, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            })
        });

        router.route("/message/:idConv").get(function(req, res){
            message.getMessage(req.idUser, req.params.idConv, req.query.nbMessage, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            })
        });

        router.route("/message/:idConv").post(function(req, res){
            message.sendMessage(req.body.message, req.idUser, req.params.idConv, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            })
        });
    }
}
