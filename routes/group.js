var group = require('../models/group.js');

module.exports = {
    route: function (router) {
        router.route('/group').get(function(req, res){
            group.getGroups(req.idUser, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            });
        });

        router.route('/group/:idGroup').get(function(req, res){
            group.getGroup(req.idUser, req.params.idGroup, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            });
        });

        router.route('/group').post(function(req, res){
            group.createGroup(req.idUser, req.body.nameGroup, req.body.presentation , function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            });
        });

        router.route('/group').patch(function(req, res){
            group.updateGroup(req.idUser, req.body.idGroup, req.body.name, req.body.presentation, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            });
        })

        router.route('/group/user').post(function(req, res){
            group.addUser(req.idUser, req.body.idUser, req.body.idGroup, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            });
        });

        router.route('/group').delete(function(req, res){
           group.deleteGroup(req.idUser, req.body.idGroup, function(err, result){
               if(err)
                    res.json(err);
               else
                    res.json(result);
           })
        });

        router.route('/group/user').delete(function(req, res){
            group.deleteUser(req.idUser, req.body.idUser, req.body.idGroup, function(err, result){
                if(err)
                    res.json(err);
                else
                    res.json(result);
            });
        });

        router.route('/group/end').patch(function(req, res){
            group.endGroup(req.idUser, req.body.idGroup, function(err, result){
                if(err)
                    res.json(err)
                else
                    res.json(result)
            })
        })

        router.route('/group/note/:idUser').post(function(req,res){
            group.note(req.idUser, req.params.idUser, req.body.idGroup, req.body.note, function(err, result){
                if(err)
                    res.json(err)
                else
                    res.json(result)
            })
        })

        router.route('/group/:idGroup/remainingNote').get(function(req, res){
            group.getRemainingNote(req.idUser, req.params.idGroup, function(err, result){
                if(err)
                    res.json(err)
                else
                    res.json(result)
            })
        })

        router.route('/group/:idGroup/user/:idUser/note').get(function(req, res){
            group.getNote(req.idUser, req.params.idGroup, req.params.idUser, function(err, result){
                if(err)
                    res.json(err)
                else
                    res.json(result)
            })
        })

    }
}