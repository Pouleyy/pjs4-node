/**
 * Created by Killian on 21/01/2016.
 */
var user = require('./users.js');
var connection = require('./connection.js');
var mysql = require('../node_modules/mysql');

module.exports = {
    modifyProfil : function (idUser, profil, callback){
                if(!idUser || !profil || !profil.firstname || !profil.lastname ||
                    !profil.email || !profil.birthday || !profil.gender || !profil.picture){
                    var err = {code: 400, message: "paramètres manquants"}
                    return callback(err);
                }
                var birthday = new Date(Date.parse(profil.birthday));
                var sql = "UPDATE profil SET firstname=?, lastname=?, email=?, birthday=?, gender=?, picture=? WHERE idUser = ? ";
                var inserts = [profil.firstname, profil.lastname, profil.email, birthday, profil.gender, profil.picture, idUser];
                sql = mysql.format(sql, inserts);
                connection.query(sql, function(err, rows){
                    if(err)
                        return callback(err)
                    else {
                        rows = {code: 0};
                        callback(err, rows);
                    }
                });
            },

    getProfil : function (idUser, idUserSearch, callback){
        var sql = "SELECT * FROM profil WHERE idUser = ? AND idUser IN (SELECT idUserFrom FROM accept WHERE idUserTo = ? " +
            "AND accept =1 AND idUserFrom IN (SELECT idUserTo FROM accept WHERE idUserFrom = ? and accept = 1))"
        var inserts = [idUserSearch, idUser, idUser];
        sql = mysql.format(sql, inserts);
        connection.query(sql, function(errMatch, rowsMatch){
            if(errMatch)
                callback(errMatch)
            else if(rowsMatch.length > 0 || idUser == idUserSearch){
                sql = "SELECT * FROM profil WHERE idUser = ?";
            }
            else{
                sql = "SELECT idUser, firstname, gender, picture, birthday FROM profil WHERE idUser = ?";
            }
                inserts = [idUserSearch]
                sql = mysql.format(sql, inserts);
                connection.query(sql, function(err, rows) {
                    if (rows.length <= 0)
                        return callback({code: 404, message: "utilisateur introuvable"})
                    else {
                        sql = "SELECT idSkill FROM ownSkill WHERE idUser = ?"
                        sql = mysql.format(sql, inserts);
                        connection.query(sql, function (errSkill, rowsSkill) {
                            rows[0]['skill'] = rowsSkill;
                            callback(err, rows[0]);
                        })
                    }
                })
        });
    },

    addSkill : function(idUser, skill, callback){
                var sql = "Select idSkill FROM skill where nameSkill = ?"
                var inserts = [skill];
                sql = mysql.format(sql, inserts);

                connection.query(sql ,function(err,skill){
                    if(err){
                        callback(err);
                    }
                    else if(skill.length <= 0){
                        var err = {code: 400, message: "Aucun skill trouvé"};
                        callback(err);
                    }
                    else{
                        sql = "INSERT INTO ownSkill VALUES(?,?)";
                        inserts = [idUser, skill.valueOf(1)[0].idSkill];
                        sql = mysql.format(sql, inserts);
                        connection.query(sql, function(err, rows){
                            var ok = {code: 0};
                            callback(err, ok);
                        })
                    }

                });
            },

    deleteSkill : function(idUser, skill, callback) {
        var sql = "Select idSkill FROM skill where nameSkill = ?"
        var inserts = [skill];
        sql = mysql.format(sql, inserts);

        connection.query(sql, function (err, skill) {
            if (err) {
                callback(err);
            }
            else if (skill.length <= 0) {
                var err = {code: 400, message: "Aucun skill trouvé"};
                callback(err);
            }
            else {
                sql = "DELETE FROM ownSkill where idUser=? and idSkill=?";
                inserts = [idUser, skill[0].idSkill];
                sql = mysql.format(sql, inserts);
                connection.query(sql, function (err, rows) {
                    var ok = {code: 0};
                    callback(err, ok);
                })
            }

        });
    },
};
