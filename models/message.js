/**
 * Created by Killian on 28/03/2016.
 */
var connection = require('./connection.js');
var mysql = require('../node_modules/mysql');

module.exports = {
    createConversation : function(nameConv, callback){
        if(!nameConv)
            return callback({code : 400, message : "Paramètres manquants"})
        var sql = "INSERT INTO conversation (nameConv) values(?)"
        var inserts = [nameConv]
        sql = mysql.format(sql, inserts);
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err)
            return callback(null, {code: 200, idConv : rows.insertId})
        })
    },

    addUser : function(idConv, idUser, callback){
        if(!idConv || !idUser)
            return callback({code : 400, message : "Paramètres manquants"})
        var sql = "INSERT INTO accesConv (idConv, idUser) values(?,?)"
        var inserts = [idConv, idUser]
        sql = mysql.format(sql, inserts)
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err)
            return callback(null, {code:200})
        })
    },

    rename : function(idConv, name, callback){
        if(!idConv || !name)
            return callback({code : 400, message : "Paramètres manquants"})
        var sql = "UPDATE conversation set nameConv = ? WHERE idConv = ?"
        var inserts = [name, idConv]
        sql = mysql.format(sql, inserts)
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err)
            return callback(null, {code : 200})
        })
    },

    deleteConv : function(idConv, callback){
        if(!idConv)
            return callback({code : 400, message : "Paramètres manquants"})
        var sql = "DELETE FROM conversation WHERE idConv = ?"
        var inserts = [idConv]
        sql = mysql.format(sql, inserts)
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err)
            return callback(null, {code : 200})
        })
    },

    deleteUser : function(idConv, idUser, callback){
        if(!idConv || !idUser)
            return callback({code : 400, message : "Paramètres manquants"})
        var sql = "DELETE FROM accesConv WHERE idConv = ? AND idUser = ?"
        var inserts = [idConv, idUser]
        sql = mysql.format(sql, inserts)
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err)
            return callback(null, {code : 200})
        })
    },

    getConversations : function(idUser, callback){
        if(!idUser)
            return callback({code : 400, message : "Paramètres manquants"})
        var sql = "SELECT * FROM conversation WHERE idConv IN (SELECT idConv FROM accesConv WHERE idUser = ?)"
        var inserts = [idUser]
        sql = mysql.format(sql, inserts)
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err)
            var j = 0
            var k = 0
            var message = require("./message.js")
            if(rows.length == 0)
                return callback(null, rows)
            for(var i = 0; i < rows.length; i++){
                message.getMessage(idUser, rows[i].idConv, 1, function(errC, rowsC){
                    if(errC)
                        return callback(errC)
                    rows[k].lastMessage = rowsC[0];
                    sql = "SELECT * FROM accesConv WHERE idConv = ?"
                    inserts = [rows[k].idConv];
                    sql = mysql.format(sql, inserts)
                    k++
                    connection.query(sql, function(errAC, rowsAC){
                        if(errAC)
                            return callback(errAC)
                        console.log(sql)
                        rows[j++].participants = rowsAC
                        if(j >= rows.length)
                            return callback(null, rows)
                    })
                })
            }
        })
    },

    getMessage : function(idUser, idConv, nbMessage, callback){
        if(!idConv)
            return callback({code : 400, message : "Paramètres manquants"})
        if(!nbMessage  || isNaN(nbMessage))
            nbMessage = 10;
        isPresentInConv(idConv, idUser, function(err, isPresent){
            if(err)
                return callback(err)
            if(isPresent){
                var sql = "SELECT * from message WHERE idConv  = ? ORDER BY date DESC LIMIT ?"
                var inserts = [idConv, parseInt(nbMessage)]
                sql = mysql.format(sql, inserts)
                connection.query(sql, function(err, rows){
                    if(err)
                        return callback(err)
                    return callback(null, rows)
                })
            }
            else
                return callback({code: 404, message: "L'utilisateur ne fait pas partie du groupe"})
        })
    },

    sendMessage : function(message, idUser, idConv, callback){
        if(!message || !idConv)
            return callback({code : 400, message : "Paramètres manquants"})
        isPresentInConv(idConv, idUser, function(err, isPresent){
            if(err)
                return callback(err)
            if(isPresent){
                var sql = "INSERT INTO message (string, idConv, idUser) values(?,?,?)"
                var inserts = [message, idConv, idUser]
                sql = mysql.format(sql, inserts)
                connection.query(sql, function(err, rows){
                    if(err)
                        return callback(err)
                    return callback(null, {code : 200})
                })
            }
            else
                return callback({code: 404, message: "L'utilisateur ne fait pas partie du groupe"})
        })
    }
}

function isPresentInConv(idConv, idUser, callback){
    var sql = "SELECT idUser FROM accesConv where idUser = ? AND idConv = ?";
    var inserts = [idUser, idConv];
    sql = mysql.format(sql, inserts);
    connection.query(sql, function(err, rows){
        if(err)
            return callback(err);
        if(rows.length <= 0)
            return callback(null, false);
        else
            return callback(null, true);
    })
}