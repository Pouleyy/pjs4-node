/**
 * Created by Killian on 24/03/2016.
 */
var connection = require('./connection.js');
var mysql = require('../node_modules/mysql');

module.exports = {
    getSkill : function(callback){
        var sql = "SELECT * FROM skill";
        connection.query(sql, function(err, rows){
            if(err)
                callback(err);
            else
                callback(null, rows);
        });
    }
}