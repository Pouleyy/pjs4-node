/**
 * Created by Killian on 23/02/2016.
 */

var user = require('./users.js');
var connection = require('./connection.js');
var mysql = require('../node_modules/mysql');

module.exports = {
    getNextRelation: function (tokenFacebook, idUser, nbRelation, lastActivity, callback) {
        if (!nbRelation || isNaN(nbRelation)) {
            nbRelation = 1;
        }
        var lastActivity = new Date(lastActivity);
        var now = new Date().getTime();
        if ((now - lastActivity) > 24 * 60 * 60 * 1000)
            this.refreshRelation(tokenFacebook);
            var sql = "SELECT profil.* from relation, profil WHERE profil.idUser = relation.idUserTo AND " +
                "relation.idUserFrom = ? AND NOT EXISTS " +
                "(SELECT 1 FROM accept WHERE idUserFrom = ? and idUserTo = relation.idUserTo) ORDER BY relation DESC LIMIT ?";
        var inserts = [idUser, idUser, parseInt(nbRelation)];
        sql = mysql.format(sql, inserts);
        connection.query(sql, function (err, rows) {
            var profil = require('../models/profil.js');
            if (err)
                return callback(err);
            return callback(null, rows);
        })
    },

    accept: function(idUserFrom, idUserTo, callback){
        if(!idUserTo){
            var err = {code: 400, message: "paramètres manquants"}
            return callback(err);
        }
        else{
            var sql = "INSERT INTO accept (idUserFrom, idUserTo, accept) values(?,?, true)"
            var inserts = [idUserFrom, idUserTo];
            sql = mysql.format(sql, inserts);
            connection.query(sql, function(err, rows){
                if(err)
                    return callback(err);
                else{
                    sql = "SELECT * from accept WHERE idUserFrom = ? AND idUserTo = ? AND accept = 1"
                    inserts = [idUserTo, idUserFrom];
                    sql = mysql.format(sql, inserts);
                    connection.query(sql, function(err, rows){
                        if(err)
                            return callback(err)
                        else{
                            if(rows.length > 0){
                                var message = require('../models/message.js');
                                var profil = require('../models/profil.js');
                                profil.getProfil(idUserTo, idUserTo, function(errP1, rowsP1){
                                    if(errP1)
                                        return callback(errP1)
                                    profil.getProfil(idUserFrom, idUserFrom, function(errP2, rowsP2){
                                        if(errP2)
                                            return callback(errP2)
                                        message.createConversation(rowsP1.firstname + " " + rowsP1.lastname + ", " +
                                        rowsP2.firstname + " " + rowsP2.lastname, function(errC, rowsC){
                                            if(errC)
                                                return callback(errC)
                                            message.addUser(rowsC.idConv, idUserTo, function(errAU1, rowsAU1){
                                                if(errAU1)
                                                    callback(errAU1)
                                                message.addUser(rowsC.idConv, idUserFrom, function(errAU2, rows1U2){
                                                    if(errAU2)
                                                        return callback(errAU2)
                                                    return callback(null, {code:200, matched:true})
                                                })
                                            })
                                        })
                                    })
                                })
                            }
                            else{
                                return callback(null, {code:200, matched:false})
                            }
                        }
                    })
                }
            })
        }
    },

    refuse: function(idUserFrom, idUserTo, callback){
        if(!idUserTo){
            var err = {code: 400, message: "paramètres manquants"}
            return callback(err);
        }
        else{
            var sql = "INSERT INTO accept (idUserFrom, idUserTo, accept) values(?,?, false)"
            var inserts = [idUserFrom, idUserTo];
            sql = mysql.format(sql, inserts);
            connection.query(sql, function(err, rows){
                if(err)
                    return callback(err);
                return callback(null, rows)
            })
        }
    },

    refreshRelation : function(token, callback){
        var request = require('request');
        request({
            headers : {
                "token" : token
            },
            method : "POST",
            uri : "http://localhost:8080/api/relations"
        }, function(errReq, resReq, bodyReq){
            if(callback) {
                if (errReq){
                    return callback(errReq)
                }
                else {
                    return callback(null, bodyReq)
                }
            }
        })
    },

    getMatchs : function(idUser, callback){
        var sql = "SELECT * FROM profil WHERE idUser IN (SELECT idUserFrom FROM accept WHERE idUserTo = ? " +
            "AND accept =1 AND idUserFrom IN (SELECT idUserTo FROM accept WHERE idUserFrom = ? and accept = 1))"
        var inserts = [idUser, idUser]
        sql = mysql.format(sql, inserts);
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err);
            return callback(null, rows)
        })
    }
}

function matchExist(idUser1, idUser2, callback){
    var sql = "SELECT * from accept WHERE idUserFrom = ? AND idUserTo = ?";
    var inserts = [idUser1, idUser2];
    sql = mysql.format(sql, inserts);
    connection.query(sql, function(err, rows){
        if(err)
            return callback(err);
        if(rows._length <= 0)
            return callback(null, false)
        else{
            sql = "SELECT * from accept WHERE idUserFrom = ? AND idUserTo = ?";
            inserts = [idUser2, idUser1];
            mysql.format(sql, inserts);
            connection.query(sql, function(err, rows){
                if(err)
                    return callback(err);
                if(rows._length <= 0)
                    return callback(null, false);
                else
                    return callback(null, true);
            });
        }
    })
}