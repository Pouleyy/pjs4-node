var connection = require('./connection.js');
var mysql = require('../node_modules/mysql')
var user = require('./users.js');
var message = require('./message.js');

module.exports = {
    createGroup : function(idUser, nameGroup, presentation, callback ){
                if(!nameGroup || !presentation){
                    var err = {code : 400, message : "Paramètres manquants"};
                    return callback(err);
                }
                else{
                    message.createConversation(nameGroup, function(errConv, rowsConv){
                        var sql = "INSERT INTO groups (nameGroup, presentation, idLeader, idConv) values (?,?,?, ?)";
                        var inserts = [nameGroup, presentation, idUser, rowsConv.idConv];
                        sql = mysql.format(sql, inserts);
                        connection.query(sql, function(errGroup, rowsGroup){
                            if (errGroup) {
                                return callback(errGroup);
                            }
                            addUserGroup(idUser, idUser, rowsGroup.insertId, function(errUsGr, resUsGr){
                                    if(errUsGr)
                                        return callback(errUsGr)
                                    return callback(null, {code : 200, idGroup : rowsGroup.insertId});
                                })
                            });
                        });
                }
            },

    updateGroup : function(idUser, idGroup, groupName, presentation, callback) {
        checkIdLeader(idGroup, idUser, function (err, isLeader, idConv) {
            if (err)
                return callback({code: 400, message: "Aucun groupe trouvé"});
            if (!isLeader) {
                var err = {code: 403, message: "L'utilisateur n'est pas Leader du groupe"};
                return callback(err);
            }
            var sql;
            var inserts;
            if (groupName && presentation) {
                sql = "UPDATE groups set nameGroup = ?, presentation = ? WHERE idGroup = ?";
                inserts = [groupName, presentation, idGroup];
                sql = mysql.format(sql, inserts);
                connection.query(sql, function (err, rows) {
                    if (err)
                        return callback(err);
                    message.rename(idConv, groupName, function(errConv, rowsConv){
                        if(errConv)
                            return callback(errConv)
                        return callback(null, rowsConv)
                    })
                });
            }
            else if (groupName) {
                sql = "UPDATE groups set nameGroup = ? WHERE idGroup = ?";
                inserts = [groupName, idGroup];
                sql = mysql.format(sql, inserts);
                connection.query(sql, function (err, rows) {
                    if (err)
                        return callback(err);
                    message.rename(idConv, groupName, function(errConv, rowsConv){
                        if(errConv)
                            return callback(errConv)
                        return callback(null, rowsConv)
                    })
                });
            }
            else if (presentation) {
                sql = "UPDATE groups set presentation = ? WHERE idGroup = ?";
                inserts = [presentation, idGroup];
                sql = mysql.format(sql, inserts);
                connection.query(sql, function (err, rows) {
                    if (err)
                        return callback(err);
                    return callback({code: 200});
                });
            }
            else {
                return callback(null, {code: 400, message: "paramètres manquants"});
            }
        })
    },

    deleteGroup : function(idUser, idGroup, callback) {
        if (!idGroup) {
            var err = {code: 400, message: "Paramètres manquants"};
            return callback(err);
        }
        else {
            checkIdLeader(idGroup, idUser, function (err, isLeader, idConv) {
                if (err)
                    return callback(err);
                if (!isLeader) {
                    var err = {code: 401, message: "L'utilisateur n'est pas Leader du groupe"};
                    return callback(err);
                }
                else {
                    var sql = "DELETE FROM groups WHERE idGroup = ?";
                    var inserts = [idGroup];
                    sql = mysql.format(sql, inserts);
                    connection.query(sql, function (err, rows) {
                        if (err)
                            return callback(err);
                        message.deleteConv(idConv, function(errConv, rowsConv){
                            if(errConv)
                                return callback(errConv)
                            return callback(null, {code: 200});
                        })
                    });
                }

            })
        }
    },

    addUser : addUserGroup,

    deleteUser : function(idUser, idUserToDelete, idGroup, callback) {
        if (!idUserToDelete || !idGroup)
            return callback({code: 400, message: "Paramètres manquants"});
        checkIdLeader(idGroup, idUser, function (err, isLeader, idConv) {
            if (err) {
                return callback(err);
            }
            if (!isLeader) {
                if (idUser == idUserToDelete) {
                    isPresentInGroup(idGroup, idUserToDelete, function (err, isPresent) {
                        if (err)
                            return callback(err);
                        if (isPresent) {
                            var sql = "DELETE from accessGroup WHERE idUser = ? AND idConv = ?";
                            var inserts = [idUserToDelete, idGroup];
                            sql = mysql.format(sql, inserts);
                            connection.query(sql, function (err, rows) {
                                if (err)
                                    return callback(err)
                                message.deleteUser(idConv, idUserToDelete, function(errC, rowsC){
                                    if(errC)
                                        return callback(errC)
                                    return callback(null, {code: 200});
                                })
                            })
                        }
                        else {
                            return callback({code: 404, message: "utilisateur introuvable"})
                        }
                    })
                }
                else
                    return callback({code: 401, message: "L'utilisateur n'est pas Leader du groupe"})

            }
            else {
                if (idUser != idUserToDelete) {
                    isPresentInGroup(idGroup, idUserToDelete, function (err, isPresent) {
                        if (err)
                            return callback(err);
                        if (isPresent) {
                            var sql = "DELETE from accessGroup WHERE idUser = ? AND idGroup = ?";
                            var inserts = [idUserToDelete, idGroup];
                            sql = mysql.format(sql, inserts);
                            connection.query(sql, function (err, rows) {
                                if (err)
                                    return callback(err)
                                message.deleteUser(idConv, idUserToDelete, function(errC, rowsC){
                                    if(errC)
                                        return callback(errC)
                                    return callback(null, {code: 200});
                                })
                            })
                        }
                        else
                            return callback({code : 404, message:"utilisateur introuvable"})
                    })
                }
                else {
                    return callback({code: 401, message: "Le leader ne peut pas se supprimer de son groupe"})
                }
            }
        })
    },

    endGroup : function(idUser, idGroup, callback){
        if(!idGroup)
            return callback({code: 400, message: "Paramètres manquants"});
        checkIdLeader(idGroup, idUser, function(err, isLeader){
            if(err)
                return callback(err)
            if(!isLeader)
                return callback({code: 401, message: "Seul le leader peut mettre fin à un groupe"})
            else{
                var sql = "UPDATE groups set finish = 1 WHERE idGroup = ?"
                var inserts = [idGroup]
                sql = mysql.format(sql, inserts);
                connection.query(sql, function (err, rows) {
                    if (err)
                        return callback(err)
                    return callback(null, {code: 200});
                })
            }
        })
    },

    getGroups : function(idUser, callback){
        var sql = "SELECT * FROM groups WHERE groups.idGroup IN (SELECT idGroup from accessGroup where idUser = ?)";
        var inserts = [idUser];
        sql = mysql.format(sql, inserts);
        connection.query(sql, function(err, rows){
            var j = 0;
            if(!rows || rows.length == 0)
                return callback(null, [])
            for(var i = 0; i < rows.length; i++){
                if(rows[i].finish == 1)
                    rows[i].finish = true
                else
                    rows[i].finish = false
                sql = "SELECT * from accessGroup WHERE idGroup = ? "
                inserts = [rows[i].idGroup]
                sql = mysql.format(sql, inserts);
                connection.query(sql, function(errAccess, rowsAccess){
                    if(!errAccess){
                        rows[j].contributors = rowsAccess
                        j++
                    }
                    if(j >= rows.length)
                        callback(err, rows);
                })
            }
        });
    },

    getGroup : function(idUser, idGroup,callback){
        var sql = "SELECT * FROM groups WHERE idGroup = ?";
        var inserts = [idGroup];
        sql = mysql.format(sql, inserts);
        connection.query(sql, function(err, rows){
            var j = 0;
            if(!rows || rows.length == 0)
                return callback(null, [])
            for(var i = 0; i < rows.length; i++){
                if(rows[i].finish == 1)
                    rows[i].finish = true
                else
                    rows[i].finish = false
                sql = "SELECT * from accessGroup WHERE idGroup = ? "
                inserts = [rows[i].idGroup]
                sql = mysql.format(sql, inserts);
                connection.query(sql, function(errAccess, rowsAccess){
                    if(!errAccess){
                        rows[j].contributors = rowsAccess
                        j++
                    }
                    if(j >= rows.length)
                        callback(err, rows[0]);
                })
            }
        });
    },

    note : function(idUserFrom, idUserTo, idGroup, note, callback){
        if(!idUserTo || !idGroup || !note)
            return callback({code: 400, message: "Paramètres manquants"});
        else if(note > 5 || note < -5)
            return callback({code: 500, message: "note invalide"})
        else if(idUserFrom == idUserTo)
            return callback({code: 500, message: "Vous ne pouvez pas vous noter vous même"})
        else{
            isFinished(idGroup, function(errFinish, finish){
                if(errFinish)
                    return callback(errFinish)
                else if(!finish)
                    return callback({code: 401, message: "Projet non fini"})
                else{
                    isPresentInGroup(idGroup, idUserFrom, function(errFrom, fromIsPresent){
                        if(errFrom)
                            return callback(errFrom)
                        else if(!fromIsPresent)
                            return callback({code: 401, message: "Vous ne faites pas partie du groupe"})
                        else{
                            isPresentInGroup(idGroup, idUserTo, function(errTo, toIsPresent){
                                if(errTo)
                                    return callback(errTo)
                                else if(!toIsPresent)
                                    return callback({code: 404, message: "Utilisateur introuvable das le groupe"})
                                else{
                                    var sql = "INSERT INTO note (idUserFrom, idUserTo, idGroup, note) values(?,?,?,?)"
                                    var inserts = [idUserFrom, idUserTo, idGroup, note]
                                    sql = mysql.format(sql, inserts);
                                    connection.query(sql, function (err, rows) {
                                        if (err)
                                            return callback(err)
                                        return callback(null, {code: 200});
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    },

    getRemainingNote : function(idUser, idGroup, callback){
        var sql = "SELECT idUser from accessGroup WHERE idGroup = ? AND idUser <> ?";
        var inserts = [idGroup, idUser]
        sql = mysql.format(sql, inserts)
        connection.query(sql, function(errAG, rowsAG){
            if(errAG)
                return callback(errAG)
            var j = 0
            if(rowsAG.length == 0)
                return callback(null, {remaining: false})
            for(var i = 0; i < rowsAG.length; i++){
                sql = "SELECT note FROM note WHERE idUserFrom = ? AND idUserTo = ? AND idGroup = ?"
                inserts = [idUser, rowsAG[i].idUser, idGroup]
                sql = mysql.format(sql, inserts)
                connection.query(sql, function(errN, rowsN){
                    j++;
                    if(errN)
                        return callback(errN)
                    if(rowsN.length == 0){
                        return callback(null, {remaining: true})
                    }
                    else if(j >= rowsAG.length){
                        return callback(null, {remaining: false})
                    }
               })
            }
        })
    },

    getNote : function(idUserFrom, idGroup, idUserTo, callback){
        var sql = "SELECT * FROM note WHERE idUserFrom = ? AND idUserTo = ? AND idGroup = ?"
        var inserts = [idUserFrom, idUserTo, idGroup]
        sql = mysql.format(sql, inserts)
        connection.query(sql, function(err, rows){
            if(err)
                return callback(err)
            if(rows.length == 0)
                return callback(null, {code: 404, message:"Note non trouvée", idUserFrom : idUserFrom, idUserTo : idUserTo, idGroup : idGroup})
            callback(null, rows[0])
        })
    }
}

function addUserGroup(idUser, idUserToAdd, idGroup, callback) {
    if (!idGroup || !idUserToAdd)
        return callback({code: 400, message: "Paramètres manquants"});
    checkIdLeader(idGroup, idUser, function (err, isLeader, idConv) {
        if (err)
            return callback(err);
        if (!isLeader)
            return callback({code: 401, message: "L'utilisateur n'est pas le leader du groupe"});
        else {
            var sql = "INSERT INTO accessGroup values(?,?)";
            var inserts = [idUserToAdd, idGroup];
            sql = mysql.format(sql, inserts);
            connection.query(sql, function (err, rows) {
                if (err) {
                    if (err.code == "ER_DUP_ENTRY") {
                        return callback({code: 403, message: "L'utilisateur est déjà présent dans le groupe"});
                    }
                    return callback(err);
                }
                else {
                    message.addUser(idConv, idUserToAdd, function(errAddUs, rowsAddUs){
                        if(errAddUs)
                            return callback(errAddUs)
                        return callback(null, {code: 200, idUser: idUserToAdd, idGroup: idGroup})
                    })
                }
            })
        }
    });
}

function isFinished(idGroup, callback){
    var sql = "SELECT finish FROM groups where idGroup = ?";
    var inserts = [idGroup];
    sql = mysql.format(sql, inserts);
    connection.query(sql, function(err, rows){
        if(err)
            return callback(err);
        else if(rows.length <= 0)
            return callback({code: 404, message:"Groupe inexistant"})
        else if(rows[0].finish == 0)
            return callback(null, false);
        else
            return callback(null, true);
    })
}

function checkIdLeader(idGroup, idUser, callback){
    var sql = "SELECT * FROM groups where idGroup = ?";
    var inserts = [idGroup];
    sql = mysql.format(sql, inserts);
    connection.query(sql, function(err, rows){
        if(err)
            return callback(err);
        if(rows.length <= 0) {
            var err = {code: 400, message: "Aucun groupe trouvé"};
            return callback(err);
        }
        if(rows[0].idLeader == idUser)
            return callback(null, true, rows[0].idConv);
        else
            return callback(null, false, rows[0].idConv);
    })
}

function isPresentInGroup(idGroup, idUser, callback){
    var sql = "SELECT idUser FROM accessGroup where idUser = ? AND idGroup = ?";
    var inserts = [idUser, idGroup];
    sql = mysql.format(sql, inserts);
    connection.query(sql, function(err, rows){
        if(err)
            return callback(err);
        if(rows.length <= 0)
            return callback(null, false);
        else
            return callback(null, true);
    })
}
