/**
 * Created by Killian on 21/01/2016.
 */

var connection = require('./connection.js');
var mysql = require('../node_modules/mysql');
var facebook = require('../facebook/request.js')

module.exports = {
    deleteUser : function (idUser, callback){
                var sql = "DELETE FROM users WHERE idUser = ?";
                var inserts = [idUser];
                sql = mysql.format(sql, inserts);
                console.log(sql);
                connection.query(sql, function(err, rows){
                    var res = {code: 0}
                    return callback(err, res);
                });
            },

    createUser : function (idFacebook, token, profil, callback){
        this.getUserByIdFacebook(profil.id, function(errGetUser, res){
            if(errGetUser && errGetUser.code != 404)
                return callback(errGetUser);
            else if(res.length > 0){
                return callback({code: 401, message:"utilisateur déjà existant"});
            }
            var sqlUser = "INSERT INTO users (tokenFacebook, idFacebook,lastActivity) Values(?, ?, ?)";
            var date = new Date();
            var inserts = [token, profil.id, date];
            sqlUser = mysql.format(sqlUser, inserts);
            connection.query(sqlUser, function(err, rows){
                if(err) {
                    return callback(err, null);
                }
                var sqlProfil = "INSERT INTO profil Values(?, ?, ?, ?, ?, ?, ?)";
                var birthday;
                if(!profil.birthday)
                    birthday = new Date()
                else
                    birthday = new Date(Date.parse(profil.birthday)); //Format date : yyyy-dd-mmThh:mm:ss
                var idUser = rows.insertId;
                var picture = profil.picture.data.url;
                var inserts = [idUser, profil.first_name, profil.last_name, profil.email, birthday, profil.gender, picture];
                sqlProfil = mysql.format(sqlProfil, inserts);
                connection.query(sqlProfil, function(err, rowsProfil){
                    if(err){
                        console.log(err);
                        connection.query("DELETE FROM users WHERE idUser = "+idUser, function(errDelete, rows) {
                            return callback(err, null)
                        })
                    }
                    else {
                        updateRelation(idUser, function(err, rows){
                            if(err)
                                return callback(err);
                            var response = {code : 201, message : "Compte crée"};
                            callback(err, response);
                        });

                    }
                });
            });
        })
    },

    noteUser : function (idUserFrom, idUserTo, note, callback){
        var sql = "UPDATE note set note = ? WHERE idUserFrom = ? AND idUserTo = ?";
        var inserts = [note, idUserFrom, idUserTo];

        sql = mysql.format(sql, inserts);

        connection.query(sql, function(err, rows){
            callback(err, rows);
        });
    },

    getUserByToken : function(tokenFacebook, callback) {
        if(!tokenFacebook){
            var err = {code : 4, message: "Token manquant"};
            callback(err);
            return;
        }
        facebook.getProfil(tokenFacebook, function (errFb, profilFb) {
            if (profilFb.error) {
                return callback(profilFb.error);
            }
            else{
                var user = require('../models/users.js');
                user.getUserByIdFacebook(profilFb.id, function(err, rows){
                    if(err)
                        return callback(err)
                    else{
                        user.updateToken(rows[0].idUser, rows[0].tokenFacebook, tokenFacebook, function(errUpdate, rowsUpdate){
                            if(errUpdate)
                                return callback(errUpdate)
                            else
                                return callback(null, rows[0])
                        })

                    }
                })
            }
        })
    },

    getUserByIdFacebook : function (idFacebook, callback){
        if(!idFacebook){
            var err = {code: 400, message: "idFacebook non fourni"};
            callback(err);
        }
        else {
            var sql = "SELECT * FROM users WHERE idFacebook = ?";
            var inserts = [idFacebook];
            sql = mysql.format(sql, inserts);
            connection.query(sql, function (err, rows) {
                if(rows.length <= 0)
                    return callback({code:404, message:"Aucun utilisateur associé à ce compte Facebook"}, rows)
                else
                    return callback(err, rows);
            })
        }
    },

    updateToken : function (idUser, tokenFacebookOld, tokenFacebookNew,callback){
        if(!idUser || !tokenFacebookOld || !tokenFacebookNew){
            var err = {code: 503, message: "Bad Request"};
            callback(err);
        }
        else if(tokenFacebookOld != tokenFacebookNew){
            var sql = "UPDATE users set tokenFacebook = ? WHERE idUser = ?";
            var inserts = [tokenFacebookNew, idUser];
            sql = mysql.format(sql, inserts);
            connection.query(sql, function (err, rows) {
                    return callback(err, rows);
            })
        }
        else{
            return callback(null, null)
        }
    },

};

function updateRelation(idUser, callback){
    var sqlnote = "INSERT INTO relation (IdUserFrom, IdUserTo, relation) SELECT IdUser, '"+idUser+"', '0' FROM users WHERE IdUser <> '"+idUser+"'";
    connection.query(sqlnote, function(err, rows) {
        var sqlnote = "INSERT INTO relation (IdUserFrom, IdUserTo, relation) SELECT '"+idUser+"', IdUser, '0' FROM users WHERE IdUser <> '"+idUser+"'";
        connection.query(sqlnote, function(err, rows) {
            return callback(err, rows);
        });
    });
}

