var FB = require('../node_modules/fb');

module.exports = {
    getProfil : function (token, callback) {
        facebookConnect(token, function (err, fb) {
            fb.api('/me', { fields: ['id', 'first_name', 'last_name', 'email', 'gender', 'birthday', 'picture.height(500).width(500).type(large)'] } ,function (res) {
                callback(null, res);
            });
        });
    }
}

function facebookConnect(token, callback) {
    var fb = FB;
    fb.setAccessToken(token);
    callback(null, fb);
};
